extends Spatial


var initrndtime : float = 0.0 
var initrndtone : float = 0.0 

func _ready():
	#randomize the pitch and start time of the sound so they are disynchronized
	#start a timer with random time 0..1 sec and set a tone number
	randomize()
	initrndtime = randf()
	initrndtone = randf()
	$Timer.wait_time = initrndtime
	$Timer.start()


func _on_Timer_timeout():
	$RigidBody/AudioStreamPlayer3D.play()
	$RigidBody/AudioStreamPlayer3D.pitch_scale = 0.5 + initrndtone

